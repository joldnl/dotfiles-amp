#!/bin/bash

# Install and activate atom packages from packages.txt
echo ""
echo "-------------------------------------------"
echo "| Install & Configure Apache"
echo "-------------------------------------------"

#
echo ""
echo "Install Xcode-select:"
xcode-select  —install

brew tap homebrew/php

sudo apachectl stop

echo ""
echo "Unlink build in apache from os x"
sudo launchctl unload -w /System/Library/LaunchDaemons/org.apache.httpd.plist 2>/dev/null

echo ""
echo "Install Apache"
brew install httpd

echo ""
echo "Start apache from brew install"
sudo brew services start httpd

# Force apache restart
sudo apachectl -k restart

echo ""
echo "Install php56, php70, php71, php72"
brew install php56 --with-httpd && brew unlink php56
brew install php70 --with-httpd && brew unlink php70
brew install php71 --with-httpd && brew unlink php71
brew install php72 --with-httpd

echo ""
echo "Install SPHP"
curl -L https://gist.github.com/w00fz/142b6b19750ea6979137b963df959d11/raw > /usr/local/bin/sphp
chmod +x /usr/local/bin/sphp

# Export path variables
export PATH=/usr/local/bin:/usr/local/sbin:$PATH


echo ""
echo "Create sites folder and add index.php"
mkdir ~/Sites
echo "<?php phpinfo(); ?>" > ~/Sites/index.php

echo ""
echo "Copy httpd.conf:"
cp -Rf httpd.conf /usr/local/etc/httpd/httpd.conf

# Force apache restart
sudo apachectl -k restart

echo ""
echo "Install php apcu and opcache:"
sphp 56 && brew install php56-opcache php56-apcu --build-from-source
sphp 70 && brew install php70-opcache php70-apcu --build-from-source
sphp 71 && brew install php71-opcache php71-apcu --build-from-source
sphp 72 && brew install php72-opcache php72-apcu --build-from-source

# Force apache restart
sudo apachectl -k restart

echo ""
echo "Install php yaml:"
sphp 56 && brew install php56-yaml --build-from-source
sphp 70 && brew install php70-yaml --build-from-source
sphp 71 && brew install php71-yaml --build-from-source
sphp 72 && brew install php72-yaml --build-from-source

echo ""
echo "Install Xdebug:"
sphp 56 && brew install php56-xdebug --build-from-source
sphp 70 && brew install php70-xdebug --build-from-source
sphp 71 && brew install php71-xdebug --build-from-source

echo ""
echo "Install xdebug-osx:"
brew install xdebug-osx

echo ""
echo "Install MailHog:"
brew install mailhog
echo "Mailhog installed. Access mailhog trough http://localhost:8025/"

echo "Done Installing Apache and PHP!"



echo ""
echo "-------------------------------------------"
echo "| Install Mysql"
echo "-------------------------------------------"

echo ""
echo "Install mariadb:"
brew install mariadb

echo ""
echo "Start mariadb as a service:"
brew services start mariadb

echo ""
echo "Run secure installation script:"
/usr/local/bin/mysql_secure_installation

echo ""
echo "Install PhpMyAdmin:"
brew install phpmyadmin

# Force apache restart
sudo apachectl -k restart

echo "Done Installing Mysql!"
